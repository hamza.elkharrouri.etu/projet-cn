#!/usr/bin/env python
# coding: utf-8

# In[59]:


#nom du fichier: spline.py

#Importation
import numpy as np

#Definition du jeu de données
x=[0,2,4,5,8,10]
y=[-1,1,6,0,2,5]

#Pour (n+1) à interpoler, on fixe la valeur n; ici on a n=5:
n = 5

#Paramètre de lissage (modifiable):
p = 0.00001

#Vecteur h:
h = np.ones(n)
for i in range(0,n):
    h[i] = x[i+1] - x[i]

#Matrice T:
T = np.zeros((n-1,n-1),dtype=float)
for i in range (0,n-1):
    T[i][i] = 2*(h[i]+h[i+1])
for j in range (0,n-2):
    T[j+1][j] = h[j+1]
    T[j][j+1] = h[j+1]
T = T/3

#Matrice G:
G = np.ones(n)
for i in range(0,n):
    G[i] = 1/h[i]
    
#Matrice Q:
Q = np.zeros((n+1,n-1),dtype=float)
for i in range(0,n-1):
    Q[i][i] = G[i]
    Q[i+1][i] = -G[i]-G[i+1]
    Q[i+2][i] = G[i+1]
    
#Définition de A,B et sigma
Qt = np.transpose(Q)
Sigma = np.eye(n+1) #sigma = identité
Sigma2 = Sigma @ Sigma
A = Qt @ Sigma2 @ Q + p * T
B = p * Qt @ y
    
#Definition de la méthode de Cholesky
from math import sqrt
def cholesky(A):
    n = len(A)
    L = np.zeros((n,n),dtype=float)
    for i in range(n):
        for k in range(i+1):
            somme = sum(L[i][j] * L[k][j] for j in range(k))
            if (i == k): 
                L[i][k] = sqrt(A[i][i] - somme)
            else:
                L[i][k] = (1.0 / L[k][k] * (A[i][k] - somme))
    return L

#Matrice triangulaire L
L = cholesky(A)

#Résolution du système linéaire:
from scipy.linalg import solve_triangular

w = solve_triangular(L, B,lower=True)
Lt = np.transpose(L)
c = solve_triangular(Lt,w)

#Construction du vecteur a:
a = y -((1/p)*(Sigma2@Q@c))

#Constructions des vecteurs b et d:
cnew = np.zeros(n+1)
for i in range(0,n-1):
    cnew[i+1]=c[i]
b = np.ones(n)
d = np.ones(n)
for i in range(0,n):
    d[i] = ((cnew[i+1]-cnew[i]))/(3*h[i])
    b[i] = ((a[i+1]-a[i])/h[i]) - cnew[i]*h[i] - d[i]*(h[i]**2)

#Affichage de la spline:
import matplotlib.pyplot as plt

for i in range(n):
    xnew = np.arange(x[i], x[i+1], abs(x[i+1]-x[i])/1000)
    plt.plot(xnew, a[i] + (b[i]*(xnew-x[i]))+ (cnew[i]*((xnew-x[i])**2)+(d[i]*((xnew-x[i])**3))))
   

plt.plot(x,y,'ro')
plt.xlabel("xi")
plt.ylabel("yi")
plt.title("Spline pour p=...")
plt.show()
