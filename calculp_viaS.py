#!/usr/bin/env python
# coding: utf-8

# In[18]:


#nom du fichier: calculp_viaS.py

p = 0.1 #changer la valeur de p ici pour tester le code (dans cet exemple on fixe arbitrairement 0.1)
S = 0.002 

#Importation
import numpy as np

#Definition du jeu de données
x=[0,2,4,5,8,10]
y=[-1,1,6,0,2,5]

#Pour (n+1) à interpoler, on fixe la valeur n; ici on a n=5:
n = 5

#Paramètre de lissage (arbitraire):
p = 9999

#Vecteur h:
h = np.ones(n)
for i in range(0,n):
    h[i] = x[i+1] - x[i]

#Matrice T:
T = np.zeros((n-1,n-1),dtype=float)
for i in range (0,n-1):
    T[i][i] = 2*(h[i]+h[i+1])
for j in range (0,n-2):
    T[j+1][j] = h[j+1]
    T[j][j+1] = h[j+1]
T = T/3

#Matrice G:
G = np.ones(n)
for i in range(0,n):
    G[i] = 1/h[i]
    
#Matrice Q:
Q = np.zeros((n+1,n-1),dtype=float)
for i in range(0,n-1):
    Q[i][i] = G[i]
    Q[i+1][i] = -G[i]-G[i+1]
    Q[i+2][i] = G[i+1]
    

# Calcul de p :
eps = 0.00000000001 
p_ancien = p #stockage des anciennes valeurs de p
condition_arret = 0 
Qt = np.transpose(Q)
Sigma = np.eye(n+1) #sigma = identité
Sigma2 = Sigma @ Sigma
print ( " p = " ,p )


while (condition_arret==0 ):
    Qt = np.transpose(Q)
    p_ancien = p
    u = np.linalg.inv( Qt@Sigma2@Q + p * T ) @Qt@y # @ = Multiplication entre matrice
    15
    uT = np.transpose(u)
    v = Sigma@Q@u
    vT = uT@Qt@Sigma
    Fp2 = vT@v
    Fp = np . sqrt ( Fp2 )
    FP2Prime = 2 * (( p * uT @ T @ np.linalg.inv( Qt@Sigma2@Q + p * T ) @T@u ) - uT@T@u )
    fp = (1/ Fp ) - (1/ np . sqrt ( S ))
    fpp = ( -1/2*( FP2Prime )*1/ Fp )/ Fp2
    p = p -( fp / fpp )
    print ( " p = " ,p )
    if(abs ( p_ancien - p ) < eps ):
        condition_arret = 1

   


# In[ ]:




